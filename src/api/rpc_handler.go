package rpc_handler

import (
	"context"
	"encoding/json"
	handler "gabber/handler/api/proto"
	"log"
	"net/http"
	"os"
	"time"

	"google.golang.org/grpc"
)

type GRPC_handler struct {
	cli handler.ManagerClient
	con *grpc.ClientConn
}

func (g *GRPC_handler) Connect() (err error) {
	host := os.Getenv("MANAGER_SERVER")
	port := os.Getenv("MANAGER_PORT")
	g.con, err = grpc.Dial(host+":"+port, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	g.cli = handler.NewManagerClient(g.con)
	log.Printf("%+v", g.cli)
	return
}

func (g *GRPC_handler) Disconnect() {
	g.con.Close()
}

func (g *GRPC_handler) OnClientConnection(w http.ResponseWriter, r *http.Request) {
	log.Printf("%+v", g.cli)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	rs, err := g.cli.OnClientConnection(ctx, &handler.AuthInfo{SN: "testing"})
	if err != nil {
		log.Fatalf("SEND REQ ERR %v", err)
	}
	log.Println("SEND REQ SUX: ", rs)
}

func (g *GRPC_handler) AddNewSMS(w http.ResponseWriter, r *http.Request) {
	var sms = &handler.NewSMS{}
	decoder := json.NewDecoder(r.Body)
	decoder.Decode(sms)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	rc, err := g.cli.OnNewSMS(ctx, &handler.NewSMS{
		Client: "testing",
		SMS:    sms.SMS})
	if err != nil {
		log.Fatalf("SEND REQ ERR %v", err)
	}
	log.Println("SEND REQ SUX: ", rc)
}

func (g GRPC_handler) jsonDecoder(r *http.Request, i interface{}) interface{} {
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&i)
	if err != nil {
		panic(err)
	}
	return i
}
