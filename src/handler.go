package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	ApiHandler "gabber/handler/api"

	"github.com/gorilla/mux"
)

func main() {
	routeHandle := &ApiHandler.GRPC_handler{}
	routeHandle.Connect()
	router := mux.NewRouter()

	router.HandleFunc("/register",
		routeHandle.OnClientConnection).Methods("POST")

	router.HandleFunc("/newsms",
		routeHandle.AddNewSMS).Methods("POST")

	router.HandleFunc("/probe",
		selfDiagnost).Methods("GET")

	http.Handle("/", router)
	err := http.ListenAndServe(":"+os.Getenv("WEB_PORT"), nil)
	if err != nil {
		log.Fatal(err)
	}
}

// Пока заглушка
func selfDiagnost(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "OK")
}
