module gabber/handler

go 1.16

require (
	github.com/golang/protobuf v1.5.2
	github.com/gorilla/mux v1.8.0
	google.golang.org/grpc v1.38.0
	google.golang.org/protobuf v1.26.0
)
